<?php

return [
    'Safe2Pay' => [
        'urls' => [
            'payment' => 'https://payment.safe2pay.com.br/v2/Payment',
        ],
        'test' => false,
        'application' => 'Doação Pastoral da Pessoa Idosa',
        'vendor' => 'Pastoral da Pessoa Idosa',
        'reference' => 'Campanha de Doação',
        'sandbox' => [
            'token' => '129590C8B61B45C084807244C265FA48',
            'secretKey' => '9C00EDA11D664A5D8E074A2A21EA8EB6F4A7EFAB3B9742C1B58CDC9DA46C54B2'
        ],
        'release' => [
            'token' => '05DC5648CE804F2A938315788E29A90E',
            'secretKey' => '83C3A5787A47465B8BAA5C33E2D66455655FD65E005B4EC58A98FF13E48B0781'
        ],
    ]
];