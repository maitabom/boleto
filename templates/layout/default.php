<!DOCTYPE html>
<html>

<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        Doação
    </title>
    <?= $this->Html->meta('icon') ?>

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.1/normalize.css">

    <?= $this->Html->css('bootstrap/bootstrap.min.css') ?>
    <?= $this->Html->css('main.css') ?>
</head>

<body>
    <main class="main">
        <div class="container">
            <header>
                <div class="row">
                    <div class="col-2">
                        <?=$this->Html->image('logo.jpeg', ['alt' => 'Pastoral da Pessoa Idosa', 'class' => 'logo']);?>
                    </div>
                    <div class="col-10">
                        <!--<h1>Faça sua doação</h1>-->
                    </div>
                </div>
            </header>
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
        </div>
    </main>
    <?= $this->Html->script('jquery.min.js') ?>
    <?= $this->Html->script('jquery.mask.min.js') ?>
    <?= $this->Html->script('bootstrap/bootstrap.min.js') ?>
    <?= $this->Html->script('main.js') ?>
</body>

</html>
