<div class="card">
    <h1 class="card-header">Boleto Gerado com Sucesso</h1>
    <div class="card-body text-center">
        <h4 class="card-title">O boleto foi gerado com sucesso</h4>
        <p><?=h($mensagem)?></p>
        <a href="<?=$boleto?>" target="_blank">Clique aqui para imprimir o boleto</a>
        <p>Data de Vencimento: <?=h($vencimento)?></p>
        <p>Código de Barras: <br /><?=h($barcode)?></p>
        <?= $this->Html->link('Nova Doação', ['action' => 'index'], ['class' => 'btn btn-primary']) ?>
    </div>
</div>