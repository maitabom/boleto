<div class="card">
    <h1 class="card-header"> Ocorreu um problema ao efetuar a transação</h1>
    <div class="card-body text-center">
        <p><?=h($mensagem)?></p>
        <?= $this->Html->link('Voltar', ['action' => 'index'], ['class' => 'btn btn-primary']) ?>
    </div>
</div>