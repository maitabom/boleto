<?php
echo $this->Form->create(null, [
    "url" => [
        "controller" => "doacao",
        "action" => "process",
    ],
    "role" => "form"]);
?>
<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            <label for="documento">CPF</label>
            <?=$this->Form->text('documento', ['id' => 'documento', 'class' => 'form-control', 'required' => true])?>
        </div>
        <div class="form-group">
            <label for="nome">Nome/Razão Social</label>
            <?=$this->Form->text('nome', ['id' => 'nome', 'class' => 'form-control', 'required' => true])?>
        </div>
        <div class="form-group">
            <label for="email">E-mail</label>
            <?=$this->Form->email('email', ['id' => 'email', 'class' => 'form-control', 'required' => true])?>
        </div>
        <div class="form-group">
            <label for="celular">Celular</label>
            <?=$this->Form->tel('celular', ['id' => 'celular', 'class' => 'form-control', 'required' => true])?>
        </div>
        <div class="row">
            <div class="col-8">
                <div class="form-group">
                    <label for="endereco">Endereço</label>
                    <?=$this->Form->text('endereco', ['id' => 'endereco', 'class' => 'form-control', 'required' => true])?>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="numero">Número</label>
                    <?=$this->Form->text('numero', ['id' => 'numero', 'class' => 'form-control', 'required' => true])?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="complemento">Complemento</label>
                    <?=$this->Form->text('complemento', ['id' => 'complemento', 'class' => 'form-control'])?>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="bairro">Bairro</label>
                    <?=$this->Form->text('bairro', ['id' => 'bairro', 'class' => 'form-control', 'required' => true])?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="cidade">Cidade</label>
                    <?=$this->Form->text('cidade', ['id' => 'cidade', 'class' => 'form-control', 'required' => true])?>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="estado">Estado</label>
                    <?=$this->Form->select('estado', $estados, ['id' => 'estado', 'class' => 'form-control', 'empty' => 'Selecione', 'required' => true])?>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="cep">CEP</label>
                    <?=$this->Form->text('cep', ['id' => 'cep', 'class' => 'form-control', 'required' => true])?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="doacao">Valor da Doação</label>
            <?=$this->Form->text('doacao', ['id' => 'doacao', 'class' => 'form-control text-right', 'required' => true])?>
            <small class="form-text text-muted">Para DOAR R$ 100,00 (cem reais), digite apenas assim: 100 (Sem pontos ou
                vírgulas).</small>
        </div>

        <button type="submit" class="btn btn-primary btn-lg btn-block">Continuar com a Doação</button>
    </div>
</div>
<?php
echo $this->form->end();