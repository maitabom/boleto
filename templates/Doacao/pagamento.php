<div class="card">
    <h1 class="card-header"> Agradecemos Pela Colaboração</h1>
    <div class="card-body text-center">
        <h4 class="card-title">O pagamento foi autorizado com sucesso</h4>
        <p><?=h($mensagem)?></p>

        <p>Cartão: <?=h($cartao)?></p>
        <p>Transação: <?=h($transacao)?></p>

        <?= $this->Html->link('Nova Doação', ['action' => 'index'], ['class' => 'btn btn-primary']) ?>
    </div>
</div>