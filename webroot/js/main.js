$(function () {
    $('#documento').mask('000.000.000-00');
    $('#celular').mask('(00) 00000-0000');
    $('#cep').mask('00000-000');
    $('#doacao').mask('#.##0', {
        reverse: true,
    });

    $('#numero_cartao').mask('0000 0000 0000 0000');
    $('#validade_cartao').mask('00/0000');
    $('#cvv').mask('000');

    $("form").submit(function () {
        var submit = $(this).find("button[type='submit']");

        submit.prop('disabled', true);
        submit.prop('title', 'Os dados estão sendo enviados ao servidor. Por favor, aguarde!');
        submit.css('cursor', 'wait');
    });

    $("input[type='radio']").change(function () {

        formaPagamento = $("input[type='radio']:checked").val();

        if (formaPagamento == "1") {
            $('#nome_cartao').attr('required', true);
            $('#numero_cartao').attr('required', true);
            $('#validade_cartao').attr('required', true);
            $('#cvv').attr('required', true);

            $("#card").show();
        } else {
            $('#nome_cartao').removeAttr('required');
            $('#numero_cartao').removeAttr('required');
            $('#validade_cartao').removeAttr('required');
            $('#cvv').removeAttr('required');

            $("#card").hide();
        }
    });
});
