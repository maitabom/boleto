<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\Http\Client;
use Cake\I18n\FrozenTime;

/**
 * Classe de manipulação e integração com a API do Safe2Pay
 */
class SafepayComponent extends Component
{
    /**
     * Entra em contato com a Safe2Pay para geração do boleto
     * @param array $data Conjunto de dados enviados por usuários, necessários para geração do boleto
     * @return mixed Retorno da API para geração do boleto.
     */
    public function gerarBoleto($data)
    {
        $url = Configure::read('Safe2Pay.urls.payment');
        $sandbox = Configure::read('Safe2Pay.test');
        $application = Configure::read('Safe2Pay.application');
        $vendor = Configure::read('Safe2Pay.vendor');
        $reference = Configure::read('Safe2Pay.reference');
        $token = $sandbox ? Configure::read('Safe2Pay.sandbox.token') : Configure::read('Safe2Pay.release.token');
        $dueDate = FrozenTime::now()->addDays(1);

        $params = [
            'IsSandbox' => $sandbox,
            'Application' => $application,
            'Vendor' => $vendor,
            'PaymentMethod' => "1",
            'Reference' => $reference,
            'Customer' => [
                'Name' => $data['nome'],
                'Identity' => $data['documento'],
                'Phone' => $data['celular'],
                'Email'=> $data['email'],
                'Address' => [
                    'ZipCode' => $data['endereco']['cep'],
                    'Street' => $data['endereco']['endereco'],
                    'Number' => $data['endereco']['numero'],
                    'Complement' => $data['endereco']['complemento'],
                    'District' => $data['endereco']['bairro'],
                    'CityName' => $data['endereco']['cidade'],
                    'StateInitials' => $data['endereco']['estado'],
                    'CountryName' => $data['endereco']['pais']
                ]
            ],
            'Products' => [
                [
                    'Code' => '001',
                    'Description' => 'Doação',
                    'UnitPrice' => $data['valorDoacao'],
                    'Quantity' => 1
            ]],
            'PaymentObject' => [
                'DueDate' => $dueDate->i18nFormat('dd/MM/yyyy'),
                'Instruction' => 'Pagável em qualquer agência bancária.',
                'IsEnablePartialPayment' => false,
                'CancelAfterDue' => false,
            ]
        ];
        
        $result = $this->post($url, $token, $params);
        return $result;
    }

    /**
     * Entra em contato com a Safe2Pay para registro e pagamento via cartão
     * @param array $data Conjunto de dados enviados por usuários, necessários para registro e pagamento via cartão
     * @return mixed Retorno da API do pagamento.
     */
    public function efetuarPagamento($data)
    {
        $url = Configure::read('Safe2Pay.urls.payment');
        $sandbox = Configure::read('Safe2Pay.test');
        $application = Configure::read('Safe2Pay.application');
        $vendor = Configure::read('Safe2Pay.vendor');
        $reference = Configure::read('Safe2Pay.reference');
        $token = $sandbox ? Configure::read('Safe2Pay.sandbox.token') : Configure::read('Safe2Pay.release.token');
        $dueDate = FrozenTime::now()->addDays(10);

        $params = [
            'IsSandbox' => $sandbox,
            'Application' => $application,
            'Vendor' => $vendor,
            'PaymentMethod' => "2",
            'Reference' => $reference,
            'Customer' => [
                'Name' => $data['nome'],
                'Identity' => $data['documento'],
                'Phone' => $data['celular'],
                'Email'=> $data['email'],
                'Address' => [
                    'ZipCode' => $data['endereco']['cep'],
                    'Street' => $data['endereco']['endereco'],
                    'Number' => $data['endereco']['numero'],
                    'Complement' => $data['endereco']['complemento'],
                    'District' => $data['endereco']['bairro'],
                    'CityName' => $data['endereco']['cidade'],
                    'StateInitials' => $data['endereco']['estado'],
                    'CountryName' => $data['endereco']['pais']
                ]
            ],
            'Products' => [
                [
                    'Code' => '001',
                    'Description' => 'Doação',
                    'UnitPrice' => $data['valorDoacao'],
                    'Quantity' => 1
            ]],
            'PaymentObject' => [
                'Holder' => $data['cartao']['nome'],
                'CardNumber' => $data['cartao']['numero'],
                'ExpirationDate' => $data['cartao']['validade'],
                'SecurityCode' => $data['cartao']['cvv'],
                'InstallmentQuantity' => $data['parcelas']
            ]
        ];
        
        $result = $this->post($url, $token, $params);
        return $result;
    }

    /**
     * Função de envio de dados ao servidor
     * @param string $url URL da Endpoint do servidor
     * @param string $token Chave token requerida do servidor
     * @param array $params Array de parâmetros
     * @return mixed Resposta do servidor
     */
    private function post($url, $token, $params)
    {
        $opts = array(
            'http'=>array(
              'method'=>"POST",
              'header'=>"X-API-KEY: $token\r\n" .
                        "Content-type: application/json\r\n",
              'content'=> json_encode($params)
            )
        );
          
        $context = stream_context_create($opts);
        $result = file_get_contents($url, false, $context);
        return $result;
    }
}