<?php

namespace App\Controller;

use Cake\Core\Configure;
use Exception;

class DoacaoController extends AppController
{
    public function index()
    {
        $parcelas = [
            '01' => '01 parcela',
            '02' => '02 parcelas',
            '03' => '03 parcelas',
            '04' => '04 parcelas',
            '05' => '05 parcelas',
            '06' => '06 parcelas',
            '07' => '07 parcelas',
            '08' => '08 parcelas',
            '09' => '09 parcelas',
            '10' => '10 parcelas',
            '11' => '11 parcelas',
            '12' => '12 parcelas',
        ];
        
        $estados = $this->Geography->obterEstados();

        $this->set('estados', $estados);
        $this->set('parcelas', $parcelas);
    }

    public function process()
    {
        if ($this->request->is('post'))
        {
            $this->gerarBoleto();
        }
    }

    public function boleto()
    {
        $detail = $this->getRequest()->getSession()->read('Response.details');

        $this->set('mensagem', $detail->Description);
        $this->set('barcode', $detail->DigitableLine);
        $this->set('vencimento', $detail->DueDate);
        $this->set('boleto', $detail->BankSlipUrl);
    }

    public function pagamento()
    {
        $detail = $this->getRequest()->getSession()->read('Response.details');

        $this->set('mensagem', $detail->Description);
        $this->set('transacao', $detail->IdTransaction);
        $this->set('cartao', $detail->CreditCard->CardNumber);
    }

    private function gerarBoleto()
    {
        try
        {
            $data = [];

            $data['documento'] = $this->getRequest()->getData('documento');
            $data['nome'] = $this->getRequest()->getData('nome');
            $data['email'] = $this->getRequest()->getData('email');
            $data['celular'] = $this->getRequest()->getData('celular');
            $data['valorDoacao'] = $this->formatarValorDoacao($this->getRequest()->getData('doacao'));

            $data['endereco']['endereco'] = $this->getRequest()->getData('endereco');
            $data['endereco']['numero'] = $this->getRequest()->getData('numero');
            $data['endereco']['complemento'] = $this->getRequest()->getData('complemento');
            $data['endereco']['bairro'] = $this->getRequest()->getData('bairro');
            $data['endereco']['cidade'] = $this->getRequest()->getData('cidade');
            $data['endereco']['estado'] = $this->getRequest()->getData('estado');
            $data['endereco']['cep'] = $this->getRequest()->getData('cep');
            $data['endereco']['pais'] = 'Brasil';

            $result = $this->Safepay->gerarBoleto($data);

            if($result == FALSE)
            {
                $this->set('mensagem', 'Ocorreu um erro de comunicação entre o sistema e o gateway de pagamento');

                $this->render('erro');
            }
            else
            {
                $response = json_decode($result);

                if($response->HasError)
                {
                    $this->set('mensagem', $response->Error);

                    $this->render('erro');
                }
                else
                {
                    $detail = $response->ResponseDetail;

                    $this->getRequest()->getSession()->write('Response.details', $detail);
                    $this->redirect(['action' => 'boleto']);
                }
            }
        }
        catch (Exception $ex)
        {
            $this->set('mensagem', $ex->getMessage());
            $this->render('erro');
        }
    }

    private function efetuarPagamento()
    {
        try
        {
            $data = [];

            $data['documento'] = $this->getRequest()->getData('documento');
            $data['nome'] = $this->getRequest()->getData('nome');
            $data['email'] = $this->getRequest()->getData('email');
            $data['celular'] = $this->getRequest()->getData('celular');
            $data['valorDoacao'] = $this->formatarValorDoacao($this->getRequest()->getData('doacao'));
            $data['formaPagamento'] = $this->getRequest()->getData('forma_pagamento');
            $data['parcelas'] = $this->getRequest()->getData('parcelas');

            $data['endereco']['endereco'] = $this->getRequest()->getData('endereco');
            $data['endereco']['numero'] = $this->getRequest()->getData('numero');
            $data['endereco']['complemento'] = $this->getRequest()->getData('complemento');
            $data['endereco']['bairro'] = $this->getRequest()->getData('bairro');
            $data['endereco']['cidade'] = $this->getRequest()->getData('cidade');
            $data['endereco']['estado'] = $this->getRequest()->getData('estado');
            $data['endereco']['cep'] = $this->getRequest()->getData('cep');
            $data['endereco']['pais'] = 'Brasil';

            $data['cartao']['nome'] = $this->getRequest()->getData('nome_cartao');
            $data['cartao']['numero'] = $this->getRequest()->getData('numero_cartao');
            $data['cartao']['validade'] = $this->getRequest()->getData('validade_cartao');
            $data['cartao']['cvv'] = $this->getRequest()->getData('cvv');

            $result = $this->Safepay->efetuarPagamento($data);


            if($result == FALSE)
            {
                $this->set('mensagem', 'Ocorreu um erro de comunicação entre o sistema e o gateway de pagamento');

                $this->render('erro');
            }
            else
            {
                $response = json_decode($result);

                if($response->HasError)
                {
                    $this->set('mensagem', $response->Error);

                    $this->render('erro');
                }
                else
                {
                    $detail = $response->ResponseDetail;

                    $this->getRequest()->getSession()->write('Response.details', $detail);
                    $this->redirect(['action' => 'pagamento']);
                }
            }
        }
        catch (Exception $ex)
        {
            $this->set('mensagem', $ex->getMessage());
            $this->render('erro');
        }
    }

    private function formatarValorDoacao(string $valor)
    {
        $pivot = str_replace('.', '', $valor);
        return floatval($pivot);
    }
}